<?php
/*
 * RAFRAME
 * Configuration Class
 * 
 * Configuration handler
 *  
*/

/*
TODO LIST:
Have this pull from .env file and DB stuff
*/

namespace RA;

define("TEMPLATE_PATH", __DIR__ . "/../templates/");
define("TEMPLATE_ASSETS_PATH", "/"); //This will change when actual theme folders exist
define("TEMPLATE_FORMAT", ".php");
define("MODULE_PATH", __DIR__ . "/../modules/");

class Config {

    private static $instance = null;
    
    public $login_path;
    public $post_login_path;
    public $public_paths;
    public $public_header_path;
    public $header_path;
    public $public_footer_path;
    public $footer_path;

    private function __construct(){

        $this->login_path = TEMPLATE_PATH . "login" . TEMPLATE_FORMAT;
        $this->post_login_path = TEMPLATE_PATH . "dash" . TEMPLATE_FORMAT;

        $this->public_paths = [];
        $this->public_paths["DEFAULT"] = TEMPLATE_PATH . "home" . TEMPLATE_FORMAT;
        $this->public_paths['login'] = $this->login_path;
        $this->public_paths['home'] = TEMPLATE_PATH . "home" . TEMPLATE_FORMAT;

        $this->paths = [];
        $this->paths["dash"] = TEMPLATE_PATH . "dash" . TEMPLATE_FORMAT;
        $this->paths["home"] = TEMPLATE_PATH . "home" . TEMPLATE_FORMAT;

        $this->public_header_path = TEMPLATE_PATH . "header" . TEMPLATE_FORMAT;//Same for now
        $this->header_path = TEMPLATE_PATH . "header" . TEMPLATE_FORMAT;
        $this->public_footer_path = TEMPLATE_PATH . "footer" . TEMPLATE_FORMAT;//Same for now
        $this->footer_path = TEMPLATE_PATH . "footer" . TEMPLATE_FORMAT;
        
    }

    public static function get_instance(){
        if (self::$instance == null)
        {
            self::$instance = new \RA\Config();
        }
 
        return self::$instance;
    }

    
}