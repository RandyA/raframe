<?php
/*
 * RAFRAME
 * Core Class
 * 
 * Handles User Auth and redirection
 *  
*/

/*
TODO LIST:
- Proper Logging instead of using error_log
- Hook in DB Access for storage
- Remove magic strings/numbers and move to DB
- Change routing to non-specific
- Add logout option
- Add module support
-- Send Module data to Templates
*/

namespace RA;
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/storage.php';
require_once __DIR__ . '/module.php';
class Core {

    private static $instance = null;

    private $config;  //RA\Config
    private $storage;  //RA\Storage
    private $modules;

    public $accepted_ips = [];
    public $accepted_passwords = [];

    public $query_string;
    public $ipaddress;
    public $request_method;
    public $request_uri;

    public $pass = "";
    public $cookie = "";
    public $auth_hash = "";
    public $referrer = "";
    public $errors = "";

    protected function __construct(){

        $this->config = \RA\Config::get_instance();
        $this->storage = \RA\Storage::get_instance();

        $this->load_modules();

        session_start();

        $this->accepted_ips[] = "206.108.31.162";
        $this->accepted_passwords[] = "testpass";

        $this->query_string = $_SERVER['QUERY_STRING'];
        $this->ipaddress = $_SERVER['REMOTE_ADDR'];
        $this->request_method = $_SERVER['REQUEST_METHOD'];
        $this->request_uri = preg_replace("@/+@", "", $_SERVER['REQUEST_URI']);

        if ( isset($_POST['pass']) ) 
            $this->pass = $_POST['pass'];

        filter_var( $this->pass, FILTER_VALIDATE_REGEXP, array( "options"=> array( "regexp" => "/.{6,25}/")) );

        if ( isset($_COOKIE['user']) )
            $this->cookie = $_COOKIE['user'];
        
        if ( isset($_SESSION['auth_hash']) )
            $this->auth_hash = $_SESSION['auth_hash'];

        if ( isset($_SERVER['HTTP_REFERER']) )
            $this->referrer = $_SERVER['HTTP_REFERER'];
        
        if ( isset($_SESSION['errors']) )
            $this->errors = $_SESSION['errors'];

        $this->log_access();
        

        //run all modules prior to views
        $this->run_modules();

        $this->check_auth();
    }

    //Cycles through the modules folder for matching filenames
    public function load_modules(){

        $this->modules = [];

        $module_folders = glob(MODULE_PATH . 'raframe_*', GLOB_ONLYDIR);

        $module_folders_names = str_replace( MODULE_PATH . 'raframe_', "", $module_folders );

        for ($i = 0; $i < count($module_folders); $i++){
            require_once $module_folders[$i] . "/" . $module_folders_names[$i] . ".php";
            $classname = ucfirst($module_folders_names[$i]);
            $this->modules[$module_folders_names[$i]] = new $classname();
        }
    }

    public function run_modules(){
        foreach ($this->modules as $module){
            $module->_ra_immediate();
        }
    }

    public static function get_instance(){
        if (self::$instance == null)
        {
            self::$instance = new \RA\Core();
        }
 
        return self::$instance;
    }
    

    public function log_access(){
    
        $print_pass = print_r($this->pass, true);
        $print_cookie = print_r($this->cookie, true);
        $print_auth_hash = print_r($this->auth_hash, true);
        $print_referrer = print_r($this->referrer, true);
        $print_errors = print_r($this->errors, true);
        $print_request_uri = print_r($this->request_uri, true);
        $print_request_method = print_r($this->request_method, true);
        $print_ipaddress = print_r($this->ipaddress, true);
        $print_query_string = print_r($this->query_string, true);
    
        $full_post = print_r($_POST, true);
    
        $logs = <<<EOT
    --- Access Attempt --- \n
    Ip Address: $print_ipaddress \n
    Query String: $print_query_string \n
    Pass Supplied: $print_pass \n
    Cookie data: $print_cookie \n
    Hash Session: $print_auth_hash \n
    Referrer: $print_referrer \n
    Errors: $print_errors \n
    Request Uri: $print_request_uri \n
    Request Method: $print_request_method \n
    POST: $full_post \n
    --- -------------- ---\n
EOT;
    
        //error_log($logs, 3, "/var/log/apache2/ra-logs.log");
        //error_log($logs);
        self::log_out("IP Address: " . $print_ipaddress);
        self::log_out("Query String: " . $print_query_string);
        self::log_out("Password: " . $print_pass);
        self::log_out("Cookie: " . $print_cookie);
        self::log_out("AuthHash: " . $print_auth_hash);
        self::log_out("Referrer: " . $print_referrer);
        self::log_out("Errors: " . $print_errors);
        self::log_out("Request URI: " . $print_request_uri);
        self::log_out("Request Method: " . $print_request_method);
    }

    public function check_auth(){

        //Check if there is an Auth Session
        if ( !$this->check_auth_session() ){

            //No session found or mismatch, check if there is a password
            if ( !empty($this->pass) && in_array($this->pass, $this->accepted_passwords) ){
                //Password Matched Setup the Auth Session
                self::log_out("Password Matched");
                $this->setup_auth();

            } else {
                //No Matching password, check if there even was a password and send the error
                if (!empty($this->pass)){
                    $this->errors .= "Access Denied. Failure to Authenticate \n";
                }
                $this->auth_redirect();
            }

        } else {
            //There is an active session, send them through
            $this->auth_redirect(true);
        }
    }

    //Should be good
    public function check_auth_session(){

        if ($this->cookie == $this->auth_hash &&  (!empty($this->cookie) && !empty($this->auth_hash)) ){
            self::log_out("Cookie Matched");
            return true;
        } else {
            self::log_out("Cookie did not match");
            self::log_out("Cookie: " . $this->cookie);
            self::log_out("AuthHash: " . $this->auth_hash);
            return false;
        }
    }

    //should be good
    public function setup_auth(){

        $hash = self::create_auth_hash();
        $limit = time() + (86400 * 30);

        setcookie("user", $hash, $limit,"/");
        $_SESSION["auth_hash"] = $hash;

        //Pass them through
        $this->auth_redirect(true);
    }

    //Hook this into the DB to get page specific data (e.g. title)
    public function auth_redirect($passthrough = false){

        if ( $passthrough || $this->check_auth_session()){
            self::log_out("Passthrough or Session validated");

            if ( empty($this->request_uri) || $this->request_uri == "index" || $this->request_uri == "login"){
                self::log_out("Moving on from homepage or login page to dashboard");
                header("Location: /dash");

            } else if ( array_key_exists($this->request_uri, $this->config->paths) ) {
                self::log_out("Moving on to requested URL");
                $this->include_with_variables($this->config->paths[$this->request_uri], array('title' => $this->request_uri, 'errors' => $this->errors));

            } else {
                http_response_code(404);
                //include('my_404.php');
            }

        } else if ($this->request_uri != "login"){

            if (empty($this->request_uri)){
                //Send to default public page
                $this->include_with_variables($this->config->public_paths["DEFAULT"], array('title' => "default", 'errors' => $this->errors));
            } else if ( array_key_exists($this->request_uri, $this->config->public_paths) ){
                //Accessing a public page
                $this->include_with_variables($this->config->public_paths[$this->request_uri], array('title' => $this->request_uri, 'errors' => $this->errors));
            } else {
                //Accessing a non-public page, force login
                self::log_out("No Passthrough or Session - Force login");
                header("Location: /login");
            }
            
        } else {
            //Load up the login template
            $this->include_with_variables($this->config->login_path, array('title' => 'Login', 'errors' => $this->errors));
        }
        die();
    }

    public function include_with_variables( $filePath, $variables = array(), $public = false, $print = true ){
        self::log_out(__FUNCTION__ . $filePath, true);

        

        $output = NULL;
        if(file_exists($filePath)){
            // Extract the variables to a local namespace
            extract($variables);

            foreach ($this->modules as $module){
                extract( $module->_get_template_data() );
            }

            // Start output buffering
            ob_start();

            ob_clean();

            // Include the template file
            if ($public){
                include $this->config->public_header_path;
                include $filePath;
                include $this->config->public_footer_path;
            } else {
                include $this->config->header_path;
                include $filePath;
                include $this->config->footer_path;
            }

            // End buffering and return its contents
            $output = ob_get_clean();
        }
        if ($print) {
            print $output;
        }
        return $output;
    }

    

    public static function log_out($message, $error = false){
        //if ($error){
            error_log($message);
        //}
    }

    //Should be good
    public static function create_auth_hash(){
        return md5( time() );
    }



}







