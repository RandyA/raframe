<?php
/*
 * RAFRAME
 * Storage Class
 * 
 * Database Handler
 *  
*/

/*
TODO LIST:

*/

namespace RA;
class Storage {

    private static $instance = null;

    private function __construct(){

    }

    public static function get_instance(){
        if (self::$instance == null)
        {
            self::$instance = new \RA\Storage();
        }
 
        return self::$instance;
    }
}