<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_ASSETS_PATH; ?>css/styles.css?ver=<?php echo time(); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo TEMPLATE_ASSETS_PATH; ?>css/terminal/jquery.terminal.min.css?ver=<?php echo time(); ?>">
    <script src="<?php echo TEMPLATE_ASSETS_PATH; ?>js/jquery-3.3.1.min.js?ver=<?php echo time(); ?>"></script>
    <script src="<?php echo TEMPLATE_ASSETS_PATH; ?>js/terminal/jquery.mousewheel-min?ver=<?php echo time(); ?>"></script>
    <script src="<?php echo TEMPLATE_ASSETS_PATH; ?>js/terminal/jquery.terminal.min.js?ver=<?php echo time(); ?>"></script>
    <title><?php echo $title; ?></title>
</head>