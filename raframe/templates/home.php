
<style>
body {
    display: none;
    /*background: #000 url("<?php echo $unsplash_file->urls->full; ?>") center center fixed;*/
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover; 
}

.glitch__img {
    background: url("<?php echo $unsplash_file->urls->full; ?>") no-repeat 50% 0;
    background-size: cover;
}



</style>
<body>
    <div class="glitch">
		<div class="glitch__img"></div>
		<div class="glitch__img"></div>
		<div class="glitch__img"></div>
		<div class="glitch__img"></div>
		<div class="glitch__img"></div>
	</div>

    <div id="logo-nav">
        <h1 class="glitch-text" data-text="Randy Alvarez">RA</h1>
        <h2 id="terminal_icon" class="nav-icon">>_</h2>
        <h2 id="separator" class="nav-icon">|</h2>
        <h2 id="glitch_icon" class="nav-icon">GX</h2>
    </div>
    
    <div id="terminal"></div>

    <p style="display:none;" class="errors"><?php echo $errors; ?></p>
    
    <pre style="display:none;"><?php //print_r($unsplash_file->urls->full); ?></pre>
    
</body>
