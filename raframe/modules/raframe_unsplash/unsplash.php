<?php
/*
 * RAFRAME
 * Module Class
 * 
 * 
 *  
*/

/*
TODO LIST:

*/
//require __DIR__ . '/unsplash-php/vendor/autoload.php';

class Unsplash extends \RA\Module {

    public $collection_id;

    public function __construct(){
        
    }

    //This works - will fire on Module Load
    public function _ra_immediate(){
        //$this->auth_example();
    }

    public function _get_template_data(){
        //
        $file = json_decode( $this->get_unsplash_image() );
        $unsplash_file = [];
        $unsplash_file['unsplash_file'] = $file;
        return $unsplash_file;
    }

    public function get_unsplash_image(){
        $file = file_get_contents("https://api.unsplash.com/photos/random?client_id=933bad5a12fdc97149284ec8cb0bcf3492429695cff0f51212c1238cec327ae3&collections=2007467");
        return $file;
    }

    /*
    public function auth_example(){
        $code ="e419944864f4df92941304dece1a255be58b492c8b4332ef937c4f28213a3296";
        $callback = "urn:ietf:wg:oauth:2.0:oob";
        $clientId = "933bad5a12fdc97149284ec8cb0bcf3492429695cff0f51212c1238cec327ae3";
        $file = file_get_contents("https://api.unsplash.com/photos/random?client_id=933bad5a12fdc97149284ec8cb0bcf3492429695cff0f51212c1238cec327ae3");
        print_r($file);
        die();
    }*/
}