$( window ).on( "load", function() {

    //Randomize the Header Font
    var random_font_number = Math.floor((Math.random() * 2) + 1);

    switch(random_font_number) {
        /*
        case 1:
            $("h1").addClass("sega_font");
            $(".nav-icon").addClass("sega_font");
            break;
        case 2:
            $("h1").addClass("hyperspace_bold_italic_font");
            $(".nav-icon").addClass("hyperspace_bold_italic_font");
            break;
        
        case 3:
            $("h1").addClass("vectorb_font");
            break;
        case 4:
            $("h1").addClass("hyperspace_bold_font");
            break;
        case 5:
            $("h1").addClass("hyperspace_font");
            break;
        case 6:
            $("h1").addClass("hyperspace_italic_font");
            break;
        default:
            $("h1").addClass("hyperspace_bold_italic_font");
            $(".nav-icon").addClass("hyperspace_bold_italic_font");
        */
    }

    //Now show the page
    $("body").fadeIn();

    $("#glitch_icon").click(function(){
        if ( $(this).hasClass("selected") ){
            $(this).removeClass("selected");
            $(this).text("GX");
            $(".glitch-disabled").addClass("glitch");
            $(".glitch-disabled").removeClass("glitch-disabled");
        } else {
            $(this).addClass("selected");
            $(this).text("GL");
            $(".glitch").addClass("glitch-disabled");
            $(".glitch").removeClass("glitch");
        }
    });

    $("#terminal_icon").click(function(){

        if ( $(this).hasClass("selected") ){
            $(this).removeClass("selected");
            $('#terminal').hide();

            $(".glitch__img:nth-child(n+2)").show();
            $(".glitch__img:nth-child(2)").show();
            $(".glitch__img:nth-child(3)").show();
            $(".glitch__img:nth-child(4)").show();
            $(".glitch__img:nth-child(5)").show();

        } else {
            $(this).addClass("selected");
            $('#terminal').show();
       
            $('#terminal').terminal(function(command) {
                if (command !== '') {
                    try {
                        var result = window.eval(command);
                        if (result !== undefined) {
                            this.echo(new String(result));
                        }
                    } catch(e) {
                        this.error(new String(e));
                    }
                } else {
                    this.echo('');
                }
            }, {
                greetings: "RandyAlvarez.ca Terminal and Javascript Interpreter - Type 'command' for a list of commands",
                name: 'ra_term',
                prompt: 'ra> '
            });

            $(".glitch__img:nth-child(n+2)").hide();
            $(".glitch__img:nth-child(2)").hide();
            $(".glitch__img:nth-child(3)").hide();
            $(".glitch__img:nth-child(4)").hide();
            $(".glitch__img:nth-child(5)").hide();
        }
    });
    
}); 
